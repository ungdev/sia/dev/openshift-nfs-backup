#!/bin/bash -e

OPENSHIFT_DIRECTORY=${OPENSHIFT_DIRECTORY:-"/export/openshift"}
MOUNT_DIRECTORY=${MOUNT_DIRECTORY:-"/srv/backup"}
CREDENTIALS_LOCATION=${CREDENTIALS_LOCATION:-"/root/.nfs-backup"}

echo "OPENSHIFT_DIRECTORY $OPENSHIFT_DIRECTORY"
echo "MOUNT_DIRECTORY $MOUNT_DIRECTORY"
echo "CREDENTIALS_LOCATION $CREDENTIALS_LOCATION"
echo "MOUNT_DIRECTORY $MOUNT_DIRECTORY"
echo "OPENSHIFT_URL $OPENSHIFT_URL"

# Creates a directory if not exists
function try_mkdir() {
  /bin/mkdir -p $1 2> /dev/null || true
}

try_mkdir -p ${MOUNT_DIRECTORY}

if ! oc version > /dev/null; then
  echo "oc client not installed !"
  exit 1
fi

if [[ ! -f ${CREDENTIALS_LOCATION} ]]; then
  echo "Openshift credentials missing !"
  exit 1
fi

if [[ -z ${OPENSHIFT_URL} ]]; then
  echo "OPENSHIFT_URL environment variable missing"
  exit 1
fi

oc login --token="$(cat ${CREDENTIALS_LOCATION})" ${OPENSHIFT_URL}
result=$(oc get pvc -l backup=nfs --all-namespaces -o jsonpath='{range .items[?(@.status.phase == "Bound")]}{.metadata.namespace}{" "}{.metadata.name}{" "}{.spec.volumeName}{"\n"}{end}')

while read project pvc pv; do
  volume_name=${project}-${pvc}-${pv}
  source_folder="${OPENSHIFT_DIRECTORY}/${volume_name}"
  destination_folder="${MOUNT_DIRECTORY}/${volume_name}"

  echo "Mount $source_folder"

  try_mkdir ${destination_folder}
  mount --bind -o ro ${source_folder} ${destination_folder}
  
done < <(printf '%s\n' "$result")

echo "Finished !"
